(function($) {
    "use strict";
    $('.veno').venobox({
        numeratio: true,
        infinigall: true,
        titleattr: 'data-title',
    });
    $(".skill_countdown").counterUp({
        delay: 10,
        time: 700,
    });
    $('.counts').counterUp({
        time: 1400,
    });
    jQuery(".video-autoplay-true").YouTubePopUp();
    jQuery(".video-autoplay-false").YouTubePopUp({
        autoplay: 0
    });
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);
    $(window).scroll(function() {
        if ($(this).scrollTop() > 500) {
            $('.scroll_top_up').fadeIn()
        } else {
            $('.scroll_top_up').fadeOut()
        }
    });
    $('.scroll_top_up').fadeOut();
    $('.scroll_top_up').on('click', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false
    });
    $('.team_icon ').on('click', function() {
        $(this).siblings('.member_toggle').css({
            'left': '0px',
            'opacity': '1'
        });
        $(this).siblings('.team-close-icon').css({
            'left': '95%',
            'opacity': '1'
        })
    });
    $('.team-close-icon').on('click', function() {
        $(this).siblings('.member_toggle').css({
            'left': '-100%',
            'opacity': '0'
        });
        $(this).css({
            'left': '-100%',
            'opacity': '0'
        })
    });
    var team_member = $('.team_carousel');
    team_member.owlCarousel({
        loop: false,
        margin: 20,
        autoplay: false,
        dots: true,
        responsive: {
            0: {
                items: 1,
                margin: 20,
            },
            480: {
                items: 1,
                margin: 0,
            },
            501: {
                items: 2,
                margin: 20,
            },
            767: {
                items: 3,
                margin: 20
            },
            992: {
                items: 4
            }
        }
    });
    var testimonial_details = $('.testimonial_content');
    testimonial_details.owlCarousel({
        items: 1,
        loop: true,
        margin: 20,
        autoplay: true,
        dots: true,
        dotsSpeed: 500,
        smartSpeed: 1000,
        autoplayTimeout: 4000,
    });
    var image_slider = $('.section-image-slider');
    image_slider.owlCarousel({
        items: 1,
        loop: true,
        margin: 20,
        autoplay: false,
        dots: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left'>", "<i class='fa fa-angle-right'>"],
    });
    var client_img = $('.client_img');
    client_img.owlCarousel({
        loop: true,
        autoplay: true,
        dots: false,
        dotsSpeed: 500,
        smartSpeed: 800,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 2,
                margin: 20,
            },
            480: {
                items: 2,
                margin: 0,
            },
            501: {
                items: 3,
                margin: 20,
            },
            767: {
                items: 3,
                margin: 20
            },
            992: {
                items: 5
            }
        }
    });
    $('#contactform').submit(function(e) {
        e.preventDefault();
        var contact_name = $("#name").val();
        var contact_email = $("#email").val();
        var subject = $("#subject").val();
        var your_phone = $("#phone").val();
        var your_message = $("#message").val();
        $.post("sendmail.php", {
            name: contact_name,
            email: contact_email,
            phone: your_phone,
            subject: subject,
            message: your_message,
        }, function(data, status) {
            document.getElementById('msgmail').innerHTML = data
        })
    });
    $('#mailchimp').submit(function(e) {
        e.preventDefault();
        var n_email = $("#email-subscribe").val();
        $.post("mailchimp.php", {
            email: n_email,
        }, function(data, status) {
            document.getElementById('nconfirmation').innerHTML = data
        })
    });
    AOS.init({
        easing: 'ease-out-back',
        duration: 1500
    });
    $.extend($.easing, {
        easeInOutExpo: function(t, e, i, n, s) {
            return 0 == e ? i : e == s ? i + n : (e /= s / 2) < 1 ? n / 2 * Math.pow(2, 10 * (e - 1)) + i : n / 2 * (-Math.pow(2, -10 * --e) + 2) + i
        },
    });
    $(document).ready(function() {
        jQuery("#menuzord").menuzord({
            align: "right",
            indicatorFirstLevel: "<i class='fa fa-angle-down'></i>",
            indicatorSecondLevel: "<i class='fa fa-angle-right'></i>"
        });
        jQuery("#one-menu, #one-menu-alt").menuzord({
            indicatorFirstLevel: "<i class='fa fa-angle-down'></i>",
            indicatorSecondLevel: "<i class='fa fa-angle-right'></i>"
        });
        var navBottom = $(".nav-bottom").offset();
        $(window).on('scroll', function() {
            var w = $(window).width();
            var header = $('header');
            if ($(".nav-bottom").length === 0) {
                if (w > 768) {
                    if ($(this).scrollTop() > 0) {
                        header.addClass("sticky")
                    } else {
                        header.removeClass("sticky")
                    }
                }
            } else {
                if (w > 768) {
                    if ($(this).scrollTop() > navBottom.top + 100) {
                        header.addClass("sticky")
                    } else {
                        header.removeClass("sticky")
                    }
                }
            }
        });
        $('.menuzord-menu > li > a, .scroll_down a').bind('click', function(event) {
            var $anchor = $(this);
            var headerH = '48';
            $('.header').outerHeight();
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - headerH + "px"
            }, 1200, 'easeInOutExpo');
            event.preventDefault()
        });
        var map = $('#google_map');
        var myCenter = new google.maps.LatLng(53.628778, 9.948218);

        function initialize() {
            var mapProp = {
                center: myCenter,
                zoom: 13,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#333333"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }, {
                    "featureType": "administrative.locality",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#98bd29"
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dedede"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e9e9e9"
                    }, {
                        "lightness": 17
                    }]
                }]
            };
            var map = new google.maps.Map(document.getElementById("google_map"), mapProp);
            var marker = new google.maps.Marker({
                position: myCenter,
                animation: google.maps.Animation.BOUNCE,
                icon: 'images/map_marker_blue.png'
            });
            var infowindow = new google.maps.InfoWindow({
                content: "germany"
            });
            marker.setMap(map)
        }
        if (map.length) {
            google.maps.event.addDomListener(window, 'load', initialize)
        }
    });
    jQuery(window).on('load', function() {
        $('.preloader').fadeOut('slow')
    });
})(jQuery);
